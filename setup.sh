# Download corpus via cURL
curl --data "passwd-fichero=pt27xQr!49&x=8&y=11" -o PLN-MULCIA-Junio-2020-Dataset.zip https://consigna.us.es/547743/descarga

# Unzip the corpus
unzip PLN-MULCIA-Junio-2020-Dataset.zip

# Move the unziped content to a data folder
mkdir data
mv PLN-MULCIA-Junio-2020-Dataset/* data

# Delete the ziped file
rm PLN-MULCIA-Junio-2020-Dataset.zip
rm -r PLN-MULCIA-Junio-2020-Dataset/

echo 'done'
