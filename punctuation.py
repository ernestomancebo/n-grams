from data import CHECK_FILE, TEST_FILE, addPunctuation, load_file
from metrics import meassure_punctuation, verifyPunctuation


def addPunctuationBasic(test_lines):
    punctuated_lines = [addPunctuation(w) for w in test_lines]
    return punctuated_lines


def addPunctuation4gram():
    return None


if __name__ == "__main__":
    instances = 10
    check_lines = load_file(CHECK_FILE)[:instances]
    test_lines = load_file(TEST_FILE)[:instances]
    punctuated_test = addPunctuationBasic(test_lines)

    score = [verifyPunctuation(check_lines[i], punctuated_test[i])
             for i in range(0, len(check_lines))]

    score_map = meassure_punctuation(score)
    print('Errors: {}\nScore: {}'.format(score, score_map))
