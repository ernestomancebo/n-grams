from data import tokenize_s, TOKEN_LIST


def verifyPunctuation(check: str,  test: str):
    """Esta función devolverá una lista de pares, donde cada par contendrá la posición (indicada como índice en el string check) y el tipo de error. Los errores posibles son:

    ‘I’ → Insertion: en dicha posición la cadena test ha insertado un signo de puntuación erróneo

    ‘D’ → Deletion: en dicha posición la cadena test no ha introducido un signo de puntuación que sí deberá aparecer

    ‘S’ → Substitution: en dicha posición la cadena test ha modificado un signo

    Args:
        check (str): Valid string, with its propper punctuation
        test (str): Genetared string, with doublty punctuation

    Returns:
        list: list of error in form (error type, position)
    """

    tokenized_check = tokenize_s(check)
    tokenized_test = tokenize_s(test)

    verification_list = []
    pos_check = pos_test = 0
    while pos_check < len(tokenized_check):
        if tokenized_check[pos_check] == tokenized_test[pos_test]:
            pos_check = pos_check + 1
            pos_test = pos_test + 1
            continue

        # In case of a different case
        elif tokenized_check[pos_check].lower() == tokenized_test[pos_test].lower():
            verification_list = verification_list + [('S', pos_check)]
            pos_check = pos_check + 1
            pos_test = pos_test + 1
            continue

        # In case that it's a token
        # In case that it's a missing token, it's 'D'
        if tokenized_check[pos_check] in TOKEN_LIST:
            if tokenized_test[pos_test] not in TOKEN_LIST:
                verification_list = verification_list + [('D', pos_check)]
                pos_check = pos_check + 1
                continue

            # Inserted a Token instead of a word
            else:
                verification_list = verification_list + [('S', pos_check)]
                pos_check = pos_check + 1
                pos_test = pos_test + 1
                continue

        else:
            # Inserted a Token instead of a word
            if tokenized_test[pos_test] in TOKEN_LIST:
                verification_list = verification_list + [('I', pos_check)]
                pos_test = pos_test + 1
                continue

    return verification_list


def meassure_punctuation(score: list):
    """Meassures precision, recall and F1

    Precision: quantifies the number of positive class predictions that actually belong to the positive class.\n
    Recall: quantifies the number of positive class predictions made out of all positive examples in the dataset.\n
    F-Measure: provides a single score that balances both the concerns of precision and recall in one number.


    Args:
        score ([type]): [description]

    Returns:
        [type]: [description]
    """
    instances_count = len(score)
    true_possitives = 0
    false_negatives = 0
    false_positives = 0

    for s in score:
        if not s:
            true_possitives = true_possitives + 1
            continue

        errors_count = len(s)
        # False Positive: Errors of type I, S
        for e in s:
            if e == 'I' or e == 'S':
                false_positives = false_positives + 1

        # False Negatives: Errors of type D or remaining ammount of errors
        false_negatives = errors_count - false_positives

    # All punctuation are good
    precision = true_possitives / instances_count
    # Punctuation added, but were bad
    recall = true_possitives / (true_possitives + false_negatives)
    # Classification Accuracy
    f_score = (2 * precision * recall) / (precision + recall)

    return {'precision': precision, 'recall': recall, 'f_score': f_score}


if __name__ == "__main__":
    print(verifyPunctuation(
        "Hello. What's your name?",
        "Hello what's your, name?"
    ))
