import pandas as pd

# Special chars
DOT = '.'
COMMA = ','
COLOM = ':'
SEMI_COLOM = ';'
QUESTION_M = '?'
ADMIRATION_M = '!'

# Separation key
SEP = '--'

# Digit and Money wildcard
WILD_D = '%D'
WILD_M = '%M'

# Start and End flags
START_SEC = '<S>'
END_SEC = '</S>'

# For OOV words
UNK = 'UNK'

# UNK para un umbral < N apariciones

# Smoothing
# Asignar 1 a todas las probabilidades para evitar OOV trunctation (X*0)
# Si es milesima, checar si se separa.

# Special char list
TOKEN_LIST = [DOT, COMMA, COLOM, SEMI_COLOM, QUESTION_M, ADMIRATION_M]

# File names
CHECK_FILE = 'PunctuationTask.check.en'
TEST_FILE = 'PunctuationTask.test.en'
TRAIN_FILE = 'PunctuationTask.train.en'

special_dict = {}
model_df = pd.DataFrame()


def load_file(file_name):
    """Loads the given file from the ./data/ folder

    Args:
        file_name (str): File name

    Returns:
        list: List of lines in the file
    """
    with open('data/{}'.format(file_name)) as file:
        file_content = file.read().splitlines()

    return file_content


def append_start_wildcard(w):
    """Appends START_SEC wildcards twice

    Args:
        w (str): String to append the wildcards

    Returns:
        str: input w with two START_SEC appended
    """
    return '{} {} {}'.format(START_SEC, START_SEC, w)


def addPunctuation(w: str):
    """Es decir recibirá como entrada un string y devolverá como salida un string.
    Como primera versión de esta función addPunctuation se implementará un modelo que simplemente
    cambia la primera letra por mayúscula y añade al final del string de entrada un punto."""
    w = w.strip()
    w = w.capitalize()

    return w + '.'


def addPunctuation4Gram(w: str):
    """Punctuates the givven string

    Args:
        w (str): Sentence to punctuate

    Returns:
        str: punctuated str
    """
    w = w.strip()
    w = w.capitalize()
    lst_w = list(w)
    result_w = [lst_w[0]]

    # Estimate the first three
    estimated = estimate_wn(
        ' '.join([SEP, SEP] + result_w), lst_w[1], model_df)
    result_w = punctuate_after_token(estimated, lst_w[1], result_w)

    # Start guessing from the fourth token
    wn_index = 2
    while wn_index < len(lst_w):
        wn = lst_w[wn_index]

        # nothing preceds SEP
        if wn == SEP:
            result_w = result_w + [wn]
            wn_index = wn_index + 1
            continue

        # Check if it's a special case, if true then append it and continue
        wn = punctuate_special(wn)
        estimated = estimate_wn(
            ' '.join(lst_w[(wn_index-3):lst_w]), wn, model_df)
        result_w = result_w + [estimated]
        result_w = punctuate_after_token(estimated, wn, result_w)
        wn_index = wn_index + 1

    # Estimate the phrase ending, biased to a dot
    estimated = estimate_wn(' '.join(lst_w[(wn_index-3):lst_w]), DOT, model_df)
    result_w = result_w + [estimated]

    return ' '.join(result_w)


def punctuate_special(w: str):
    """Punctuates the given token if apply

    Args:
        w (str): Token to punctuate if apply

    Returns:
        str: Transformation result
    """
    # If it's money
    if w[0] == '$':
        return punctuate_money(w)

    # if it's a special word (alternated case, which moslty happen on composed words)
    sp_w = special_dict.get(w.lower(), False)
    if sp_w:
        return sp_w

    return w


def punctuate_after_token(estimated: str, wn: str,  result_w: list):
    """If a punctuation is estimated, check if wn must be capitalized or not

    Args:
        estimated(str): stimated w
        wn(str): wn to be predicted
        result_w(list): list that contains all the estimated w

    Returns:
        list: modified result_w
    """
    if estimated in TOKEN_LIST:
        # if ',' or ';' then keep wp as lower case, otherwise upper case
        if estimated == COMMA or estimated == SEMI_COLOM:
            result_w = result_w + wn
        else:
            result_w = result_w + wn.capitalize()

    return result_w


def tokenize_w(w: str) -> list:
    """Tokenizes the given w, including the special tokens as part of the tokens

    Args:
        w (str): Provided string.

    See Also:
    ---
        TOKEN_LIST

    Returns:
        str: A string with the special characters as words
    """
    w = w.strip()
    # TODO: Check if this is needed in cleansing
    # w = append_start_wildcard(w)
    w_arr = w.split()

    # This one holds the tokenized words, latter is joined
    return_arr = []

    for single_w in w_arr:
        # If it's a digit or currency, use the wildcards
        if single_w.isdigit():
            return_arr = return_arr + [WILD_D]
            continue

        if single_w[0] == '$':
            return_arr = return_arr + [WILD_M]
            continue

        single_w_arr = list(single_w)
        last_char = single_w_arr[-1]

        # If contains a token, separate it and use it as two tokens
        if last_char in TOKEN_LIST:
            return_arr = return_arr + \
                [''.join(single_w_arr[:-1]), single_w_arr[-1]]
            continue

        return_arr = return_arr + [''.join(single_w_arr)]

    return return_arr


def generate_n_gram(w: str, n: int):
    """Generates a n-gram for a given w sequence

    Args:
        w (string): Sequence used to generate the n-gram
        n (int): Size of the n-gram

    Returns:
        list: list of n-gram
    """
    ngrams_list = []
    for i in range(0, len(w)):
        splt_w = tokenize_w(w[i]).split()

        # In the very rare case that there's a single word
        if len(splt_w) == 1:
            ngrams_list = ngrams_list + [START_SEC, START_SEC, splt_w[0]]
            continue

        # Mocks a n-gram for the begining of a sentence,
        # used for the 3-gram in a future
        ngrams_list = ngrams_list + \
            [START_SEC, START_SEC, splt_w[0], splt_w[1]]

        for j in range(0, len(splt_w)):
            ngram = ' '.join(splt_w[j:j + n])
            ngrams_list = ngrams_list + [ngram]

    return ngrams_list


def count_wp(wp: str, df: pd.DataFrame = model_df):
    """Counts the occurence of wp

    Args:
        wp(str): Lookup sequence
        df(pd.DataFrame, optional): n-gram model. Defaults to model_df.

    Returns:
        int: wp count
    """
    wp_sum = df[df['tri_gram'] == wp]['count'].sum()
    return wp_sum


def count_wn(wn: str, df: pd.DataFrame = model_df):
    """Counts the appearance of the wn candidate

    Args:
        wn (str): wn candidate
        df (pd.DataFrame, optional): n-gram model. Defaults to model_df.

    Returns:
        int: wn count
    """
    return df[df['n_grams'] == wn]['count'].sum()


# TODO: review
def estimate_wn(wp: str, wn: str, df: pd.DataFrame = model_df):
    """Returns the best candidate of wn

    Args:
        wp (str): Previous secuence
        wn (str): Candidate w
        df (pd.DataFrame, optional): n-gram model. Defaults to model_df.

    Returns:
        str: Best w candidate
    """
    wp_cnt = count_wp(wp, df)
    wn_cnt = {}
    lst_w = []

    if special_dict.get(wn, False):
        lst_w = [wn]
    elif wn[0] == '$':
        lst_w = [WILD_M]
    elif wn.isdigit():
        lst_w = [WILD_D]
    else:
        # To cover cases of names and 'I'
        lst_w = [wn, wn.capitalize()]

    # Flag to identify if the wn is unknown
    is_wn_unk = True
    # Estimate words
    for w in lst_w:
        wn_cnt[w] = count_wn(w, df) / wp_cnt
        if wn_cnt[w] > 0:
            is_wn_unk = False

    # If it's a known word and has a high value
    if not is_wn_unk and sum(wn_cnt.values()) >= 0.5:
        max_key = max(wn_cnt, key=wn_cnt.get)

        if max_key == WILD_D or max_key == WILD_M:
            max_key = wn

        return max_key

    # Estimate tokens
    for w in TOKEN_LIST:
        wn_cnt[w] = count_wn(w, df) / wp_cnt

        # There are wp that are always followed by a token, ie. "how are you -> ?"
        if wn_cnt[w] >= 0.85:
            return w

    if not is_wn_unk:
        max_key = max(wn_cnt, key=wn_cnt.get)

        if max_key == WILD_D or max_key == WILD_M:
            max_key = wn

        return max_key
    else:
        # In the case of UNK words, return the token in case of a regular case
        sigma_token = sum(wn_cnt.values())
        if sigma_token >= 0.7:
            return max(wn_cnt, key=wn_cnt.get)

        return wn


def punctuate_money(wn):
    """Adds comma delimiter to wn if it's in money format.

    Args:
        wn(str): Word to format

    Returns:
        str: Formated string, if applies
    """
    if len(wn) > 4 and wn[0] == '$':
        l_wn = list(wn)[1:]
        l_wn.reverse()
        comma_count = (len(l_wn) - 1)//3

        inserted = 1
        for _ in range(0, comma_count):
            l_wn.insert((inserted*4) - 1, COMMA)
            inserted = inserted + 1

        l_wn.reverse()
        wn = '${}'.format(''.join(l_wn))

    return wn


def fill_special_dict(w: str):
    """Fills dictionary with special words, which are upper case multi-word

    Args:
        w(str): Word to put among the special words if applies
    """
    l_w = list(w)[1:]
    for c in l_w:
        if str(c).isupper():
            special_dict[w.lower()] = w
            return


# TODO: Review
def do_training():
    train_content = load_file(TRAIN_FILE)
    ngrams = generate_n_gram(train_content[:500], 4)

    content_df = pd.DataFrame(ngrams, columns=["n_grams"])
    model_df = pd.DataFrame(
        {'count': content_df.groupby(['n_grams']).size()}).reset_index()

    def tri_gram(x):
        splt = x["n_grams"].split()
        return ' '.join(splt[:-1]) if len(splt) > 3 else None

    model_df['tri_gram'] = model_df.apply(tri_gram, axis=1)

    return model_df


if __name__ == "__main__":
    fill_special_dict('Al-Qaeda')
    fill_special_dict('Pakistan-Iranian')
    # print(special_df.head())
    # count_df = do_training()
    # wp_count = count_wp('</S>', count_df)
    # print(count_df.sample(10), wp_count)
