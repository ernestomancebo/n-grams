

# Case Check -> If a word tends to appear a few times, and only under an specific case (ie. a Name), handle that as name.


def next_token(w):
    """Returns a set of potential next token.
    Set helps to avoid dupplication of special chars to reduce computation.

    Args:
        w (string): Word to be guessed if next

    Returns:
        set: Set of potential next tokens
    """
    return None


def predict(wp, wn):
    """Returns the probability that the next word (wn) appears right after the previous (wp): p(wn|wp)

    Args:
        wp (string): Word Previous
        wn (string): Word Next

    Returns:
        float: probability of appearance
    """
    return 1


print(next_token('casa'))
print(next_token('huevo'))
print(next_token('500'))
print(next_token('.'))
print(next_token('.'))
